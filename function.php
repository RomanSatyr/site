<?php
require_once "db.php";

function getPost($param) {
    if(!isset($_POST[$param])){
        return '';
    }
    return strip_tags(addslashes(htmlspecialchars($_POST[$param])));
}