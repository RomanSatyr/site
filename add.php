<?php

class Add
{
    public function go()
    {
        session_start();

        require_once "db.php";
        require_once "function.php";

        $phone = getPost('phone');
        $email = getPost('email');
        $birth = getPost('birth');

        $error_phone = "";
        $error_email = "";
        $error_birth = "";
        $error_files = "";
        $error = false;

        if ($email == "" || !preg_match("/@/", $email)) {
            $error_email = "Введите корректный email";
            $error = true;
        }

        if ($phone == "" || !preg_match('/^\+[0-9]{7,11}$/', $phone)) {
            $error_phone = "Введите корректный телефон";
            $error = true;
        }
        if ($birth == "") {
            $error_birth = "Введите корректную дату рождения";
            $error = true;
        }
        if (!$error) {
            $mysql = getDB();
            $result = mysqli_query($mysql, "INSERT INTO `autor` (id, phone, email, date_birth) VALUES (NULL,'$phone','$email','$birth' ) ");
            $latest_id = $mysql->insert_id;


            $uploaddir = 'file/';

            if (!is_dir($uploaddir)) mkdir($uploaddir, 0777);
            $filesize = filesize($_FILES['upload_image']['tmp_name']);

            foreach ($_FILES as $file) {

                if (move_uploaded_file($file['tmp_name'], $uploaddir . basename($file['name']))) {
                    $fileName = $uploaddir . $file['name'];
                    $result = mysqli_query($mysql, "INSERT INTO `images` (autor_id, image) VALUES ('$latest_id', '$fileName' )");

                } else {
                    $error_files = true;
                }
                if ($filesize > 1024 * 1024){
                    $error_files = "Файл превышает размер 1 МБ";
                }
            }
        }
        $error = [
            'error_phone' => $error_phone,
            'error_email' => $error_email,
            'error_birth' => $error_birth,
            'error_files' => $error_files
        ];

        echo json_encode($error);
    }
}

$run = new Add();
$run->go();



