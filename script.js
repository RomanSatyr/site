var files;

$('input[type=file]').change(function () {
    files = this.files;
});

$('.submit.button').click(function (event) {
    event.stopPropagation(); // Остановка происходящего
    event.preventDefault();  // Полная остановка происходящего

    var phone = $('input#phone1').val();
    var email = $('input#email1').val();
    var birth = $('input#birth1').val();

    var data = new FormData();
    data.append("phone", phone);
    data.append("email", email);
    data.append("birth", birth);

    $.each(files, function (key, value) {
        data.append(key, value);
    });
    $.ajax({
        url: 'add.php',
        type: 'POST',
        data: data,
        cache: false,
        dataType: 'json',
        processData: false, // Не обрабатываем файлы (Don't process the files)
        contentType: false, // Так jQuery скажет серверу что это строковой запрос
        success: function (respond, textStatus, jqXHR) {

            if (respond.error_phone != "") {
                $("#error_phone").text(respond.error_phone);
            }
            if (respond.error_email != "") {
                $("#error_email").text(respond.error_email);
            }
            if (respond.error_birth != "") {
                $("#error_birth").text(respond.error_birth);
            }
            if (respond.error_files != "") {
                $("#error_files").text(respond.error_files);
            }

        }
    });

    $('input#phone1').val('');
    $('input#email1').val('');
    $('input#birth1').val('');

});