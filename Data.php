<?php


class Data
{
    public function getImages()
    {
        require_once "db.php";

        $page = $_GET['page'];
        if (!$page) {
            $page = 1;
        }

        $kol = 10;
        $art = ($page * $kol) - $kol;

        $mysql = getDB();

        $result = mysqli_query($mysql, "SELECT * FROM `autor` AS a INNER JOIN `images` AS i ON a.id = i.autor_id LIMIT $art,$kol");

        if (isset($_GET['page'])) {
            $page = $_GET['page'];
        } else $page = 1;

        $res = mysqli_query($mysql, "SELECT COUNT(*) FROM `autor` AS a INNER JOIN `images` AS i ON a.id = i.autor_id ");

        $row = mysqli_fetch_row($res);
        $total = $row[0];
        $str_pag = ceil($total / $kol);
        for ($i = 1; $i <= $str_pag; $i++) {
            echo "<a href=display.php?page=" . $i . "> Страница "  . $i . " </a>";
        }
        echo '<br>';
        $article_id = [];

        while ($myrow = mysqli_fetch_assoc($result)) {


            if (in_array($myrow['id'], $article_id)) {
                $newArray[$myrow['id']]['image'][] = $myrow['image'];
            } else {
                $newArray[$myrow['id']] = [
                    'id' => $myrow['id'],
                    'image' => [$myrow['image']],
                    'phone'=>$myrow['phone'],
                ];
                $article_id[] = $myrow['id'];
            }
        }
        return $newArray;
    }
}
